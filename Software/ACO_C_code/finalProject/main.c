#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "aco.h"
#include<time.h>


struct Data
{
    float voltage;
    float current;
    char buff [1024];
    float voltageArray [141];
    float currentArray [141];
};


bool InitialiseValues(struct ACOR_Parameters *values)
{
  values->number_Ants = 10;
  values->new_Ants = 10;
  values->MaxIt = 2;
  values->bestNum = 5;
  values->t = 0.005;
  values->reference_voltage = 5;
  return true;
}


void extract_data (struct Data *data)
{
   char *information = strtok(data->buff,",");
   //printf("printing information buffer : %s\n",information);
    int count = 0;
    char * ampss;
    char * voltss;
    voltss = information;
    //printf("the voltage: %s \n",voltss);
    data->voltage = atof(voltss);
    //printf("the voltage in float is: %f \n",data->voltage);
   while( information != NULL )
   {
        //printf( " relavant data:  %s\n",information);
        count ++;
        //printf("printing counter: %d \n",count);
        information = strtok(NULL,",");
        if (count == 1)
        {
            ampss = information;
            //printf(" the current : %s\n",ampss);
            //now we need to convert char into float
            data->current = atof(ampss);
            //printf("the int current value is: %f \n",data->current);
        }
   }

}


void getting_power_from_csv(struct ACOR_Parameters *values)
{
    struct Data data;
    data.current = 0;
    data.voltage = 0;

    //Opens a file for reading. The file must exist.
	FILE *fp = fopen("shading.csv","r");

    int count = 0;

    while ((getc(fp)) != EOF)
    {
        //char buff [1024];
        //we need to slow down the speed of data that is being read
        //waitFor(1);
        fgets(data.buff,1024,(FILE*)fp);
        //printf("printing data %s\n",data.buff);
        extract_data(&data);
        //printf("the voltage is: %f and current is: %f \n",data.voltage,data.current);

        //printf("the counter is: %d\n", count);
        //now we need to put all the voltage and current values into an array
        data.voltageArray[count] = data.voltage;
        data.currentArray [count] = data.current/1000;
        count = count +1;

    }
       //close the file
       fclose(fp);

    int x = 0;
    //put the votlage and current values into the struct
    for (int i = 0; i < values->number_Ants; ++i)
  {
    values->solar_data [i][1] = data.voltageArray[i];
    values ->solar_data[i][2] = data.currentArray [i];
    //x = x+1;
    printf("Voltage is: %f and current is: %f \n",values->solar_data[i][1],values ->solar_data[i][2]);
  }

//  //printing the values
//       for (int i = 0; i <140; i++)
//       {
//           printf("the voltage: %f and current : %f \n",data.voltageArray[i],
//                data.currentArray[i]);
//       }

}


int main()
{
  struct ACOR_Parameters values;
  struct empty_individual InitialAnt;
  struct bestSolutions topValues;
  struct reference_data Data;

  if (!InitialiseValues(&values))
  {
    printf("InitialiseValues failed return 0");
    return 0;
  }
  //loop that runs forever
  //getting_power(&values);
  getting_power_from_csv(&values);
  start_initial_population(&values,&InitialAnt);
  Solution_archive(&topValues, &values, &InitialAnt);
  ACO_main(&values,&InitialAnt,&topValues,&Data);
//  update_duty_cycle(&values);


  return 0;
}


