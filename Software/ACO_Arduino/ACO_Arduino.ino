
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "aco.h"
#include <Wire.h>
#include <Adafruit_INA219.h>


// instantiate adafruit power sensing unit
Adafruit_INA219 ina219;


// we need to set up the output pin for the PWM signal 
static const uint8_t PWM_PIN = PD3;   

// D = sqrt(0.9* Rout * I_mpp/V_mpp) @90% efficiency     
static const uint8_t INITIAL_D_1PANEL = 47; 
static const uint8_t INITIAL_D_2PANEL = 33;
static const uint8_t INITIAL_D_3PANEL = 27;


//need to define some states for the program to flow effectively 
enum aco_states 
  {
    //we need to first initialise the all the structs 
    initialise_state,   
    //this state we have to ge the power from PV pannels 
    get_power_state, 
    //This state is responsible for starting the initial ant population 
    start_initial_state, 
    //main aco state will run the aco algorithim 
    main_aco_state,
    //once the mpp is found we need to update the duty cycle 
    update_PWM
  };


void setup() {
  // put your setup code here, to run once:
    uint32_t    currentFrequency;
    
    setup_timer_2_pwm();
    Serial.begin(115200);
    while (!Serial) 
      {
          // will pause Zero, Leonardo, etc until serial console opens
          delay(1);
      }
    
    // Initialize the INA219. Range (32V, 1A)
    // Current Resolution: 1 LSB = 0.04 mA
    // Power Resolution: 1 LSB = 20 * 0.04 mA = 0.8 mW 
    ina219.begin();
    ina219.setCalibration_32V_1A();
    
    // enable all interrupts
    sei();  
}


void setup_timer_2_pwm() 
{
    DDRD |= (1 << PWM_PIN);          // set PD3 (OC2B) as output
    OCR2A = 159;                 // set period as 160 ticks (100kHz @ 1 prescaler, 16 MHz clk)
    OCR2B = 79;                  // set initial dutycycle as 80 ticks (50% at 160 ticks period)
    
    TCCR2A = 0;                   // clear TCCR2A register
    TCCR2B = 0;                   // clear TCCR2B register
    TCCR2A |= (1 << COM2B1);      // set timer Ch B as non-inverting PWM mode (Clear on Capture)
    
    // set timer as Fast PWM mode with OCR2A as TOP (period)
    TCCR2A |= (1 << WGM21)|(1 << WGM20);  
    TCCR2B |= (1 << WGM22);
    
    TCCR2B |= (1 << CS10);        // use 1 prescaler and enable timer
}

void turn_off_pwm() {  TCCR2B &= ~(1 << CS10); }
void turn_on_pwm()  {  TCCR2B |= (1 << CS10); }

/*
 *  @brief set the duty cycle of PWM frequency. Ranges from
 *   
 */
void set_duty_cycle(double d_cycle_percent)
{
    double duty_percent = d_cycle_percent;

    // map percentage values to timer tick values
    uint8_t duty_cycle_ticks = (duty_percent * (OCR2A + 1))/100;

    // change the PWM duty cycle
    OCR2B = duty_cycle_ticks;  
}


//the function that is responsible for initialising the values
void Initialise(ACOR_Parameters *values)
{
  //number of ants 
  values->number_Ants = 10;
  //number of ants starting at the refernce point 
  values->new_Ants = 10;
  //max number of iterations 
  values->MaxIt = 2;
  // number of best values from the results 
  values->bestNum = 5;
  values->t = 0.005;
  values->reference_voltage = 5;
}

void getPower (ACOR_Parameters *values)
{  
    static const uint8_t SAMPLE_AVERAGE = 20;
  
    double current_mA = 0;
    double load_voltage = 0;
    double power_mW = 0;

  //for ACO we need to put number of current and voltage values in to an array 
    for (int j = 0; j < values->number_Ants; j++)
    {
        //unsigned long last_time = millis();
        for (int i = 0; i < SAMPLE_AVERAGE; i++ )
        {
            current_mA += ina219.getCurrent_mA();
            power_mW += ina219.getPower_mW();
            load_voltage += ina219.getBusVoltage_V() + (ina219.getShuntVoltage_mV()/1000);
        }
        //unsigned long elapsed_time = millis() - last_time;
        //Serial.print("time taken to read power: "); Serial.print(elapsed_time); Serial.println("");
      
        current_mA /= SAMPLE_AVERAGE;
        power_mW /= SAMPLE_AVERAGE;
        load_voltage /= SAMPLE_AVERAGE;    

        //now assign voltage and current to each ant 
        values->solar_data[j][1] = load_voltage; 
        values->solar_data[j][2] = current_mA; 

        //add a delay 
        delay (1000); //1 second delay 
    }
    
}


void Duty_cycle (bestSolutions *topValues)
{
   double duty = topValues->bestDutyCycle[0]*100;
   if (duty < 90 && duty > 5) 
   {
    //set the duty cycle
     set_duty_cycle (duty);
   }
   else if (duty > 90) 
   {
    set_duty_cycle(90); 
   }
   else if (duty <5)
   {
    set_duty_cycle(5); 
   }
}


//main loop 
void loop(void) 
{
  struct ACOR_Parameters values;
  struct empty_individual InitialAnt;
  struct bestSolutions topValues;
  struct reference_data Data;

  //to start to program, first state is initialise state 
  aco_states programState = initialise_state; 

  switch(programState)
    {
        case initialise_state:
              {
              //call the function that is responsible for initialsing the structs 
              turn_on_pwm();
              Initialise(&values);
              programState = get_power_state; 
              break; 
              }
        case get_power_state: 
              {
              //Now get power from the sensor 
              getPower(&values);
              programState = start_initial_state; 
              break;
              }
       case start_initial_state: 
             {
              //starting the initial ant population, with voltage and current for each ant 
              start_initial_population(&values,&InitialAnt);
              //now we need to put the best values into the solution archive 
              Solution_archive(&topValues, &values, &InitialAnt);
              //now change the state 
              programState = main_aco_state; 
              break; 
             }
       case main_aco_state: 
             {
              //finding mpp 
              ACO_main(&values,&InitialAnt,&topValues,&Data);
              programState = update_PWM;
              break; 
             }
       case update_PWM: 
             {
              //updating the duty cycle 
              Duty_cycle(&topValues); 
              programState = get_power_state;
              break;     
             }         
    }
}
