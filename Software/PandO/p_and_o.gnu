cd "c:/Users/Portheng/Documents/fyp2/Data"
set grid
set term wxt size 1200,800
set key autotitle columnheader
set style data linespoint
set pointsize 0.5
set datafile separator ","
set xlabel "times (ms)"
set title "MPPT data" noenhanced
plot 'p_and_o_data.csv' using 1:2, '' using 1:3, '' using 1:4, '' using 1:5

plot "pso_ps.csv" using 2:4 every ::1::7,"" using 2:8 every ::1::7, "" using 2:12 every ::1::7
 plot "pso_ps.csv" using 2:4 every ::54::92,"" using 2:8 every ::54::92, "" using 2:12 every ::54::92