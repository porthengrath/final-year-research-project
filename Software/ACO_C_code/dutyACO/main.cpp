#include <iostream>
#include "aco.h"
#include <fstream>
#include <string.h>
#include <vector>
#include <stdlib.h>
using namespace std;

enum States
{
    Start_ACO,
    main_ACO_loop
};


//this function is reponsible for initialise the values
void Initialise (Ant *ants)
{
    ants->num_ants = 4;
    ants->Maxit = 2;
    ants->t = 0.05;
    ants->new_ant = 4;
}


void getPower_from_csv(Ant *ants,int temp_count)
{
   //now we need to put the releavent power into the class
   //for now lets put random values, however during the real
   //case just get the current power and put it into the array
        ants->power[temp_count] = (rand() %(12) + 5);
        cout <<"the power associated with the set duty cycle from the PV pannel: "<<ants->power[temp_count]<<endl;


}


void set_duty_cycle(double d_cycle_percent)
{
    double duty_percent = d_cycle_percent*100;

    cout<<"duty cycle %: "<<duty_percent<<endl;

    // map percentage values to timer tick values
    //uint8_t duty_cycle_ticks = (duty_percent * (OCR2A + 1))/100;

    // change the PWM duty cycle
   // OCR2B = duty_cycle_ticks;
}


int main()
{
   //create an object
   Ant ants;
   States state;
   state = Start_ACO;

   switch (state)
   {
        case (Start_ACO):
        {


               //now intilise the object
               Initialise(&ants);
               //now we are setting the initial duty cycles of the ants
               ants.setting_Duty_ants();
               //next step is to calculate the power for each preset duty cycle for each ant
               for (int i = 0; i<ants.num_ants; i++)
               {
                    set_duty_cycle(ants.dutyCycle[i]);
                    //now  get power for each different duty cycle
                    //currently just reading random values from CSV file
                    getPower_from_csv(&ants,i);
               }
               //now we can start the intial ant population
               ants.start_initial_ant_population();

               //now each ant should have pre set duty cycle and the associated power
               //change the state
               state = main_ACO_loop;
               //break;
        }

        case (main_ACO_loop):
            {
                for (int it = 0; it<ants.Maxit; it++)
                {
                    //now we are going to find the best possible duty cycle
                    ants.updating_duty_cycle();
                    //next step is to find the power associated with the best possible duty cyles
                    for (int i = 0; i<ants.num_ants; i++)
                    {
                        cout<<"the best updated duty_cycle: "<<ants.best_duty_val[i]<<endl;
                        set_duty_cycle(ants.best_duty_val[i]);
                        //now get the power for each set duty cycle
                        getPower_from_csv(&ants,i);
                    }
                    ants.updating_power();
                }
                state = Start_ACO;
                break;
            }

   }




}
