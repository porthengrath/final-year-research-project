/*
 * acoduty.cpp
 *
 *  Created on: 20 May 2019
 *      Author: Binara
 */

//#include <iostream>
#include "acoduty.h"
#include <string.h>
//#include <vector.h>
#include <stdlib.h>
#include <math.h>
//#include <cmath>
#include <time.h>

//using namespace std;

//void insertingDuty (vector <float> *duty)
//{
//    duty->push_back(0.10);
//    duty->push_back(0.5);
//    duty->push_back(0.75);
//    duty->push_back(0.9);
//}

void Ant::setting_Duty_ants()
{
     //vector <float> duty;
    // insertingDuty (&duty);

   //assign each ant duty cycle
//   for (int i=0; i<num_ants; i++)
//   {
//        dutyCycle[i] = duty[i];
//   }
	//for now let just insert some pre-set duty cycle values
	dutyCycle [0] = 0.1;
	dutyCycle [1] = 0.5;
	dutyCycle [2] = 0.75;
	dutyCycle [3] = 0.90;
}

void Ant::update_Sol (float duty [],float power_sol[])
{
  for (int i = 0; i<num_ants; i++)
  {
      best_duty_val[i] = duty[i];
      best_power_val[i] = power_sol[i];
      //cout<<"updated val: "<<best_duty_val[i]<<endl;
  }
}

 void Ant::start_initial_ant_population()
 {
//   //now we can check for the new power values
//   for (int i = 0; i<num_ants; i++)
//   {
//       cout<<"the new power is: "<<power[i]<<endl;
//   }

   //create a temp array to hold maximum value of the power
      float temp [num_ants];
      float temp1 [num_ants];
  //now put all zeros to it
      for (int i = 0; i<num_ants; i++)
      {
          temp[i] = 0;
          temp1[i] = 0;
      }
    int array_numb;

  //now next step is to sort the struct from highest value to lowest value
      for (int i = 0; i<num_ants; ++i)
      {
          for (int j =0; j<num_ants; ++j)
          {
              if (temp[i] >= power[j] )
              {
                  //printf("the value is bigger than previous \n");
                  continue;
              }
              else if (temp[i] < power[j])
              {
                  temp[i] = power[j];
                  temp1[i] = dutyCycle[j];
                  array_numb = j;
                  //printf(" the cost %f \n", temp[i]);
              }
          }
           // next we need to remove the highest value from the array
        for (int x = array_numb; x < num_ants-1; x++)
        {
           power[x] = power[x+1];
           dutyCycle[x] = dutyCycle[x+1];
        }
      }

       //now that the power is arranged from min to max we can save it back to the orginal
      //array
      for (int x = 0; x<num_ants; x++)
      {
          power[x] = temp[x];
          dutyCycle[x] = temp1[x];
         //cout<<"now printing the sorted values of power: "<<power[x]<<" duty: "<<dutyCycle[x]<<endl;
      }

      update_Sol(dutyCycle,power);

 }

double GenerateRandom(double min, double max)
{
    static bool first = true;
    if (first)
    {
        srand(time(NULL));
        first = false;
    }
    if (min > max)
    {
        //std::swap(min, max);
    	//unfortunately i am not able to use STD functions, therefore I have to manually swap the min and max values
    	double temp_min = min;
    	double temp_max = max;
    	min = temp_max;
    	max = temp_min;
    }
    return min + (double)rand() * (max - min) / (double)RAND_MAX;
}



 void Ant::updating_duty_cycle()
 {
        float best_duty [num_ants];
        //now we need to calculate the distance between each best solution

        for (int i = 0; i < num_ants; i++)
        {
          best_duty[i] = fabs( best_duty_val[i]- best_duty_val[0]);
         // cout<<"distance value: "<<best_duty[i]<<endl;
        }

        //next step is to create an array that hold Guassian values for each position
        float Guassian_Duty [num_ants];

        //now calculate the Guassian value for each distance
        for (int i = 0; i<num_ants; i++)
        {
            Guassian_Duty[i] = exp(-(pow(best_duty[i],2))/(2*t));
           // cout <<"the Guassian value: "<<Guassian_Duty[i]<<endl;
        }

        //summing all the guassian values
        float sum_Guassian_duty = 0;

        for (int i = 0; i <num_ants; i++)
        {
            sum_Guassian_duty = sum_Guassian_duty + Guassian_Duty[i];
        }

        //now we need to create an array that hold phermone values for each solution
        float Phermone_duty [num_ants];

        for (int i = 0 ; i <num_ants; i++)
        {
            Phermone_duty[i] = Guassian_Duty[i]/sum_Guassian_duty;
            //cout<<"the phermone level: "<< Phermone_duty[i]<<endl;
        }

        for (int i = 0; i<num_ants; i++)
        {
            position_ant[i] = Phermone_duty[i] * new_ant;
            duty_position[i] = best_duty_val[i];
            //cout<<"number of new ants each position: "<<position_ant[i]<<" position of Duty: "<< dutyCycle[i]<<endl;
        }
        for (int i = 0; i<num_ants; i++)
        {
            //just round the numbers
            position_ant[i] = round (position_ant[i]);
            //cout<<"the new rounded value: "<<position_ant[i]<<endl;
        }
        int start = 0;
        //now starting the new ant population
        for (int i = 0; i<new_ant; i++)
        {
            //int numberOfAnt = position_ant [i] + start;
            int numberOfAnt,check;
            //numberOfAnt = position_ant [i] + start;
            check = position_ant [i];
            //cout<<"number of ants starting: "<<numberOfAnt<<endl;
            if (check > 0)
            {
                numberOfAnt = position_ant [i] + start;
                for (int j = start; j<numberOfAnt; j++)
                {
                     newDuty[j] = duty_position[i]+ GenerateRandom(-0.09,+0.09);
                     //cout<<"the new duty cycle is: "<<newDuty[j]<<" the reference point is: "<<duty_position[i]<<endl;
                }
                start = start + numberOfAnt;
            }
        }


      //update_Sol(newDuty,power);
 }


void Ant::updating_power()
{
    //now we can check for the new power for the upated duty cycles
        for (int i = 0; i<new_ant; i++)
        {
            //cout<<"the new power value: "<<power[i]<<endl;
        }
    //next step is to compare with the previous best solutions and  save the best solutions in to the archive
        float temp_archive_power [8];
        float temp_archive_Duty [8];

    //now combine the best value archive and the updated power archive
        //int length_of_temp = sizeof(temp_archive_power)/sizeof(float);
        int counterX = 0;
        for (int i = 0; i<num_ants; i++)
        {
            temp_archive_power [i] = updated_power[i];
            temp_archive_Duty [i] = newDuty[i];
            //cout<<"temp archive: "<<temp_archive_power[i]<<" and the associated duty cycle: "<<temp_archive_Duty[i]<<endl;
            counterX++;
        }
        for (int i = 0; i<num_ants; i++)
        {
            temp_archive_power[counterX] = best_power_val [i];
            temp_archive_Duty [counterX] = best_duty_val [i];
            counterX++;
        }

//        for (int i = 0; i <length_of_temp; i++)
//        {
//            cout<<"combined value power: "<<temp_archive_power[i]<<" and the associated duty: "<<temp_archive_Duty[i]<<endl;
//        }


    //now temp archive should contain the best values from the previous iteration and the updated
    //power values from the current iterations

        //next step is to sort the array from max to min power values;
        float temp_sort_power[8];  //temp array that hold max to min power values
        float temp_sort_duty [8];  //temp array that hold max to min duty cycle values

        //need to put zeros to all the temp arrays
        for (int i = 0; i<8; i++)
        {
            temp_sort_power[i] = 0;
            temp_sort_duty[i] = 0;
        }

        int Max_position;
        //size of the array that hold updated power data
        int length = sizeof(temp_archive_power)/sizeof(float);
        for (int i = 0; i <length; i++)
        {
            //now make sure to update the size of the array that hold the updated data
            int length = sizeof(temp_archive_power)/sizeof(float);

            for (int j = 0; j<8; j++)
            {
                if (temp_sort_power[i] >= temp_archive_power[j])
                {
                    //do nothing
                }
                else if (temp_sort_power[i]< temp_archive_power[j])
                {
                    temp_sort_power [i] = temp_archive_power [j];
                    temp_sort_duty [i] = temp_archive_Duty [j];
                    Max_position = j;
                }
            }
            //now we need to remove the max value from the updated solution
            for (int x = Max_position; x<length; x++)
            {
                temp_archive_power[x] = temp_archive_power[x+1];
                temp_archive_Duty[x] = temp_archive_Duty[x+1];
            }
            //cout <<"the new temp sorted values are: "<< temp_sort_power[i]<<endl;
        }

        //now update the best solution
        update_Sol(temp_sort_duty,temp_sort_power);

}


