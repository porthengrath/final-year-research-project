/*
 * pso.cpp
 *
 *  Created on: 16 May 2019
 *      Author: Portheng
 */
#include "pso.h"
#include "stdlib.h"

static const uint8_t SAMPLE_AVERAGE = 20;
static const uint16_t SLEEP_TIME = 1000;
static const uint16_t REINIT_CHECK_TIME = 1000;

static bool print_time_out()
{
    static unsigned long last_time = 0;
    unsigned long current_time = millis();

    if (current_time - last_time > SLEEP_TIME)
    {
        last_time = current_time;
        return true;
    }
    else
    {
        return false;
    }
}

static bool reinit_check_time_out()
{
    static unsigned long last_time = 0;
    unsigned long current_time = millis();

    if (current_time - last_time > REINIT_CHECK_TIME)
    {
        last_time = current_time;
        return true;
    }
    else
    {
        return false;
    }
}

pso::pso(pwm* pwm_timer,    Adafruit_INA219* ada_ina219)
{
    pwm_timer_2         = pwm_timer;
    pso_state           = state_initialise;
    global_best_voltage = 0;
    global_best_current = 0;
    global_best_power   = 0;
    global_best_duty_cycle = 0;
    ina219              = ada_ina219;
    iteration_count     = 0;
    convergence_count   = 0;
    inertia = 0.99;
}

void pso::initialise()
{
    global_best_duty_cycle = 0;
    global_best_current = 0;
    global_best_voltage = 0;
    global_best_power   = 0;
    iteration_count     = 0;
    inertia = 0.99;
    for (int i = 0; i < PARTICLE_NUM; i++)
    {
        pso_particle[i].voltage = 0;
        pso_particle[i].current = 0;
        pso_particle[i].power = 0;
        pso_particle[i].local_best_duty_cycle = 0;
        pso_particle[i].local_best_power = 0;
        pso_particle[i].velocity = 0;
    }
    pso_particle[0].duty_cycle = 75;
    pso_particle[1].duty_cycle = 50;
    pso_particle[2].duty_cycle = 35;
}

/*
 *  @brief print data in comma seperated value(csv) format so excel can open easily
 *
 *  @param solar_data       solar panels measured voltage, current, and power to print to console
 *  @param pwm_panel_data   pwm duty cycles
 *
 */
void pso::print_data()
{
   static bool title_printed = false;

    if (!title_printed)
    {
        Serial.print("time (ms)");
        Serial.print(",");
        Serial.print("iteration count");
        Serial.print(",");
        Serial.print("P1_Voltage (v)");
        Serial.print(",");
        Serial.print("P1_Current (mA)");
        Serial.print(",");
        Serial.print("P1_Power (mW)");
        Serial.print(",");
        Serial.print("P1_lb_power (mW)");
        Serial.print(",");
        Serial.print("P1_dutycycle (%)");
        Serial.print(",");
        Serial.print("P2_Voltage (v)");
        Serial.print(",");
        Serial.print("P2_Current (mA)");
        Serial.print(",");
        Serial.print("P2_Power (mW)");
        Serial.print(",");
        Serial.print("P2_lb_power (mW)");
        Serial.print(",");
        Serial.print("P2_dutycycle (%)");
        Serial.print(",");
        Serial.print("P3_Voltage (v)");
        Serial.print(",");
        Serial.print("P3_Current (mA)");
        Serial.print(",");
        Serial.print("P3_Power (mW)");
        Serial.print(",");
        Serial.print("P3_lb_power (mW)");
        Serial.print(",");
        Serial.print("P3_dutycycle (%)");
        Serial.print(",");
        Serial.print("Gb_Voltage (v)");
        Serial.print(",");
        Serial.print("Gb_Power (mW)");
        Serial.print(",");
        Serial.print("G_dutycycle (%)");
        Serial.print(",");
        Serial.print("Convergence_count");
        Serial.print("\n");
        title_printed = true;
    }
    else
    {
        Serial.print(millis());
        Serial.print(",");
        Serial.print(iteration_count);
        Serial.print(",");
        Serial.print(pso_particle[0].voltage);
        Serial.print(",");
        Serial.print(pso_particle[0].current);
        Serial.print(",");
        Serial.print(pso_particle[0].power);
        Serial.print(",");
        Serial.print(pso_particle[0].local_best_power);
        Serial.print(",");
        Serial.print(pso_particle[0].duty_cycle);
        Serial.print(",");
        Serial.print(pso_particle[1].voltage);
        Serial.print(",");
        Serial.print(pso_particle[1].current);
        Serial.print(",");
        Serial.print(pso_particle[1].power);
        Serial.print(",");
        Serial.print(pso_particle[1].local_best_power);
        Serial.print(",");
        Serial.print(pso_particle[1].duty_cycle);
        Serial.print(",");
        Serial.print(pso_particle[2].voltage);
        Serial.print(",");
        Serial.print(pso_particle[2].current);
        Serial.print(",");
        Serial.print(pso_particle[2].power);
        Serial.print(",");
        Serial.print(pso_particle[2].local_best_power);
        Serial.print(",");
        Serial.print(pso_particle[2].duty_cycle);
        Serial.print(",");
        Serial.print(global_best_voltage);
        Serial.print(",");
        Serial.print(global_best_power);
        Serial.print(",");
        Serial.print(global_best_duty_cycle);
        Serial.print(",");
        Serial.print(convergence_count);
        Serial.print("\n");
    }
}

void pso::get_voltage_power(int particle_number)
{

    double current_mA = 0;
    double load_voltage = 0;
    double power_mW = 0;

    //take average reading to reduce noise effect
    for (int i = 0; i < SAMPLE_AVERAGE; i++ )
    {
        current_mA += ina219->getCurrent_mA();
        power_mW += ina219->getPower_mW();
        load_voltage += ina219->getBusVoltage_V() + (ina219->getShuntVoltage_mV()/1000);
    }

    current_mA /= SAMPLE_AVERAGE;
    power_mW /= SAMPLE_AVERAGE;
    load_voltage /= SAMPLE_AVERAGE;

    pso_particle[particle_number].voltage     = load_voltage;
    pso_particle[particle_number].current     = current_mA;
    pso_particle[particle_number].power       = power_mW;
}

void pso::run_state_machine()
{
    switch (pso_state)
    {
        default:
        case state_initialise:
        case state_reinit:
        {
            initialise();
            pso_state = state_measure_v_p;
            break;
        }

        case state_measure_v_p:
        {
//            Serial.println("state measure voltage, power, lbest, duty cycle");
            for (int i = 0; i < PARTICLE_NUM; i++)
            {
                pwm_timer_2->set_duty_cycle(pso_particle[i].duty_cycle);
                delay(10);                  // allow for power transient to settle
                get_voltage_power(i);
            }
#if 0
            Serial.print("particle 0: "); Serial.print(pso_particle[0].voltage);
            Serial.print(","); Serial.print(pso_particle[0].power);
            Serial.print(","); Serial.print(pso_particle[0].local_best_power);
            Serial.print(","); Serial.println(pso_particle[0].duty_cycle);
            Serial.print("particle 1: "); Serial.print(pso_particle[1].voltage);
            Serial.print(","); Serial.print(pso_particle[1].power);
            Serial.print(","); Serial.print(pso_particle[1].local_best_power);
            Serial.print(","); Serial.println(pso_particle[1].duty_cycle);
            Serial.print("particle 2: "); Serial.print(pso_particle[2].voltage);
            Serial.print(","); Serial.print(pso_particle[2].power);
            Serial.print(","); Serial.print(pso_particle[2].local_best_power);
            Serial.print(","); Serial.println(pso_particle[2].duty_cycle);
#endif
            pso_state = state_update_pso;
            break;
        }

        case state_update_pso:
        {
//            Serial.println("state update pso particle");
            if (iteration_count < MAX_ITERATION)
            {
//                Serial.print("iteration count: ");
//                Serial.println(iteration_count);
                for (int i = 0; i < PARTICLE_NUM; i++)
                {
                    // update pbest if find better power
                    if (pso_particle[i].power > pso_particle[i].local_best_power)
                    {
                        pso_particle[i].local_best_power = pso_particle[i].power;
                        pso_particle[i].local_best_duty_cycle = pso_particle[i].duty_cycle;
                    }

                    // update global best
                    if (pso_particle[i].power > global_best_power)
                    {
                        convergence_count = 0;
                        global_best_voltage = pso_particle[i].voltage;
                        global_best_current = pso_particle[i].current;
                        global_best_power = pso_particle[i].power;
                        global_best_duty_cycle = pso_particle[i].duty_cycle;
                    }

                    // update velocity
                    pso_particle[i].velocity = inertia* pso_particle[i].velocity;
                    pso_particle[i].velocity += 1.0 *((float)rand()/(float)RAND_MAX)*(pso_particle[i].local_best_duty_cycle - pso_particle[i].duty_cycle);
                    pso_particle[i].velocity += 1.0 *((float)rand()/(float)RAND_MAX)*(global_best_duty_cycle - pso_particle[i].duty_cycle);
//                    Serial.print("particle "); Serial.print(i); Serial.print(" veloc: "); Serial.println(pso_particle[i].velocity);
                    // update position
                    pso_particle[i].duty_cycle += pso_particle[i].velocity;
                    if (pso_particle[i].duty_cycle < 2)
                    {
                        pso_particle[i].duty_cycle = 1;
                    }
                    else if (pso_particle[i].duty_cycle > 95)
                    {
                        pso_particle[i].duty_cycle = 95;
                    }

                    // check termination criterion (20mW)
                    if (global_best_power - pso_particle[i].power < MPP_POWER_DIFFERENCE)
                    {
                        convergence_count++;
                        if (convergence_count >= PARTICLE_NUM)
                        {
                            convergence_count = 0;
                            pso_state = state_sleep;
                        }
                        else
                        {
                            pso_state = state_measure_v_p;
                        }
                    }
                    else
                    {
                        pso_state = state_measure_v_p;
                    }
                }
                inertia *= 0.9;
                iteration_count++;
                convergence_count = 0;  // prevent the same particle from trigger convergence criterion
            }
            else
            {
//                Serial.println("max iteration exceeded");
                pso_state = state_sleep;
            }
            print_data();
            break;
        }

        case state_sleep:
        {
            pwm_timer_2->set_duty_cycle(global_best_duty_cycle);

            if (reinit_check_time_out())
            {
                //print_data();
                double power = 0;
                for (int i = 0; i < SAMPLE_AVERAGE; i++)
                {
                    power += ina219->getPower_mW();
                }
                power /= SAMPLE_AVERAGE;

                // reinit if more than 10% difference than global best power
                if (power > global_best_power)
                {
                    if (((power - global_best_power)/global_best_power) > 0.1)
                    {
                        pso_state = state_reinit;
                    }
                    else
                    {
                        pso_state = state_sleep;
                    }
                }
                else
                {
                    if (((global_best_power - power)/global_best_power) > 0.1)
                    {
                        pso_state = state_reinit;
                    }
                    else
                    {
                        pso_state = state_sleep;
                    }
                }
            }
            break;
        }
    }
}

