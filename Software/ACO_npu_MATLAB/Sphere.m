function z = Sphere(x,y)

    z = ((x-3)^8)/(1+(x-3)^8) + ((y-3)^4)/(1+(y-3)^4);

end