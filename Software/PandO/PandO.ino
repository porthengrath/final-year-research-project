#include <Wire.h>
#include <Adafruit_INA219.h>

// instantiate adafruit power sensing unit
Adafruit_INA219 ina219;


// we need to set up the output pin for the PWM signal 
static const uint8_t PWM_PIN = PD3;   

// D = sqrt(0.9* Rout * I_mpp/V_mpp) @90% efficiency     
static const uint8_t INITIAL_D_1PANEL = 47; 
static const uint8_t INITIAL_D_2PANEL = 33;
static const uint8_t INITIAL_D_3PANEL = 27;

// // stop MPP tracking if current and previous power difference are only this much
// static const double P_MPP_HYSTERESIS = 0.8;   // 0.8 mWatts from INA219 power bit-resolution

// static const double P_TRACK_HYSTERESIS = 0.1; // 0.1 mWatts

// static const double V_TRACK_HYSTERESIS = 0.01;   // 0.1 V

// minimum allowable step size due to timer resolution 1/160 * 100 = 0.625 
static const double STEP_SIZE = 0.625;	

// sleep time 10 ms
static const unsigned long SLEEP_TIME = 10; 

struct mppt_data
{
  float current_mA; 
  float voltage; 
  float power; 
  
  float v_prev;
  float p_prev;    
};


// duty cycle measured in percentage
struct pwm_data
{
  double d_cycle;     // limit between 10 and 90%
  double d_prev;      // previous duty cycle history
};

enum arduino_state 
{
  state_start_up,
  state_get_power,
  state_tracking,
  state_sleep,   // sleep, doesn't exist for P&O
};

/*
 *	@brief non-blocking timeout, use to perform non-blocking wait
 *
 *	@return bool, return true is timer expired. 
 *
 */
bool timeout()
{
  static unsigned long current_time = 0;
  static unsigned long last_time = 0;

  current_time = millis();
  if (current_time - last_time >= 2000) // 1500 msec period
  {
    last_time = current_time;
    return true;
  }
  else
  {
   return false; 
  }
}

void setup_timer_2_pwm() 
{
    DDRD |= (1 << PWM_PIN);          // set PD3 (OC2B) as output
    OCR2A = 159;                 // set period as 160 ticks (100kHz @ 1 prescaler, 16 MHz clk)
    OCR2B = 79;                  // set initial dutycycle as 80 ticks (50% at 160 ticks period)
    
    TCCR2A = 0;                   // clear TCCR2A register
    TCCR2B = 0;                   // clear TCCR2B register
    TCCR2A |= (1 << COM2B1);      // set timer Ch B as non-inverting PWM mode (Clear on Capture)
    
    // set timer as Fast PWM mode with OCR2A as TOP (period)
    TCCR2A |= (1 << WGM21)|(1 << WGM20);  
    TCCR2B |= (1 << WGM22);
    
    TCCR2B |= (1 << CS10);        // use 1 prescaler and enable timer
}

void turn_off_pwm() {  TCCR2B &= ~(1 << CS10); }
void turn_on_pwm()  {  TCCR2B |= (1 << CS10); }

/*
 *  @brief set the duty cycle of PWM frequency. Ranges from
 *   
 */
void set_duty_cycle(double d_cycle_percent)
{
    double duty_percent = d_cycle_percent;

    // map percentage values to timer tick values
    uint8_t duty_cycle_ticks = (duty_percent * (OCR2A + 1))/100;

    // change the PWM duty cycle
    OCR2B = duty_cycle_ticks;  
}

void setup(void) 
{
  uint32_t    currentFrequency;
  
  setup_timer_2_pwm();
  Serial.begin(115200);
  while (!Serial) {
      // will pause Zero, Leonardo, etc until serial console opens
      delay(1);
  }
  
  // Initialize the INA219. Range (32V, 1A)
  // Current Resolution: 1 LSB = 0.04 mA
  // Power Resolution: 1 LSB = 20 * 0.04 mA = 0.8 mW 
  ina219.begin();
  ina219.setCalibration_32V_1A();
  
  // enable all interrupts
  sei();  
}


/*
 *  @brief increase voltage, decrease duty cycle
 *
 *	@param double* duty_cycle_percent, 10 - 90
 *
 */
void increase_voltage(double 	&duty_cycle_percent)
{
	if (duty_cycle_percent < 5)
	{
		duty_cycle_percent = 5;
	}
	else
	{
		duty_cycle_percent -= STEP_SIZE;
	}
	set_duty_cycle(duty_cycle_percent);
}

/*
 *  @brief decrease voltage, increase duty cycle
 *
 *	@param double* duty_cycle_percent, 10 - 90
 *
 */
void decrease_voltage(double		&duty_cycle_percent)
{
	if (duty_cycle_percent > 90)
	{
		duty_cycle_percent = 90;
	}
	else
	{
		duty_cycle_percent += STEP_SIZE;
	}
	set_duty_cycle(duty_cycle_percent);
}

/*
 *	@brief print data in comma seperated value(csv) format so excel can open easily  
 *	
 * 	@param solar_data		solar panels measured voltage, current, and power to print to console
 *  @param pwm_panel_data 	pwm duty cycles
 * s
 */
void print_data(mppt_data	solar_data, pwm_data pwm_panel_data)
{	
	static bool title_printed = false;
	
	if (!title_printed)
	{
		Serial.print("time (ms)");
		Serial.print(",");
		Serial.print("Voltage (v)");
		Serial.print(",");
		Serial.print("Current (mA)");
		Serial.print(",");
		Serial.print("Power (mW)");
		Serial.print(",");
		Serial.print("Duty cycle (percent)");
		Serial.print("\n");
		title_printed = true;
  	}
  	else 
  	{
  		Serial.print(millis());
  		Serial.print(",");
  		Serial.print(solar_data.voltage);
  		Serial.print(",");
  		Serial.print(solar_data.current_mA);
  		Serial.print(",");
  		Serial.print(solar_data.power);
  		Serial.print(",");
  		Serial.print(pwm_panel_data.d_cycle);
  		Serial.print("\n");
  	}
}



/*
 * @brief get the voltage, current reading from INA219 high-side monitoring
 *	      All measurement are sampled a certain number of times to prevent transient
 *		  effect of perturbing duty cycle from affecting algorithm. 
 * 
 * @param mppt_data,  solar panels voltage, current, and power measurements to collect 
 *
 */
void getPower(mppt_data*    solar_data)
{  
  static const uint8_t SAMPLE_AVERAGE = 20;

  double current_mA = 0;
  double load_voltage = 0;
  double power_mW = 0;

  //unsigned long last_time = millis();
  for (int i = 0; i < SAMPLE_AVERAGE; i++ )
  {
    current_mA += ina219.getCurrent_mA();
    power_mW += ina219.getPower_mW();
    load_voltage += ina219.getBusVoltage_V() + (ina219.getShuntVoltage_mV()/1000);
  }
  //unsigned long elapsed_time = millis() - last_time;
  //Serial.print("time taken to read power: "); Serial.print(elapsed_time); Serial.println("");

  current_mA /= SAMPLE_AVERAGE;
  power_mW /= SAMPLE_AVERAGE;
  load_voltage /= SAMPLE_AVERAGE;
  
  solar_data->voltage     = load_voltage;
  solar_data->current_mA  = current_mA;
  solar_data->power       = power_mW;
}

/*
 * @brief   perturb and observe algorithm. Track MPP by comparing current power, voltage with
 *          previous power and previous voltage. Add Hysteresis since power, voltage measurement
 *        can be noisy. 
 *
 * @param mppt_data*  pointer to solar_data voltage, power data struct which is to be updated 
 *      pwm_data* pointer to pwm_data dutycycle struct which is to be updated
 *
 */
void PandO (mppt_data* solar_data, pwm_data* pwm_panel_data)
{
  if (solar_data->power == 0)
  {
    // no panel is connected or no sun-light
    //Serial.println("no solar panel is connected");
  }
  else if (solar_data->power >= solar_data->p_prev)
  {
    // we are on the left hand of MPP curve, go forward!
    if (solar_data->voltage >= solar_data->v_prev)
    {
      //dSerial.println("+delta P, + delta V, increase V");
      // increase voltage
      increase_voltage(pwm_panel_data->d_cycle);
    }
    // we are on the right hand of MPP curve, go back!
    else 
    {
      //Serial.println("+delta P, -- delta V, decrease V");
      decrease_voltage(pwm_panel_data->d_cycle);
    }

  }
  else
  {
    // we are on the right hand of MPP curve, go back!
    if (solar_data->voltage >= solar_data->v_prev)
    {
      //Serial.println("-delta P, + delta V, decrease V");
      // decrease voltage
      decrease_voltage(pwm_panel_data->d_cycle);
    }
    // we are on the left hand of MPP curve, go forward!
    else
    {
      //Serial.println("-delta P, - delta V, increase V");
      increase_voltage(pwm_panel_data->d_cycle);
    }
  }

  #if 0
  {
  	Serial.print("time : "); Serial.print(millis()); Serial.println(" ms");
  	Serial.print("voltage: "); Serial.print(solar_data->voltage); Serial.println(" v");
  	Serial.print("power: "); Serial.print(solar_data->power); Serial.println(" mW"); 
  }
  #endif

  #if 0
  {
    Serial.print("time : "); Serial.print(millis()); Serial.println(" ms");
    Serial.print("current  V:  "); Serial.print(solar_data->voltage); Serial.print(" V");
    Serial.println("");
    Serial.print("previous V: "); Serial.print(solar_data->v_prev); Serial.print(" V");
    Serial.println("");
    Serial.print("Current  I: "); Serial.print(solar_data->current_mA); Serial.print(" mA");
    Serial.println("");
    Serial.print("current  P: "); Serial.print(solar_data->power); Serial.print(" mW");
    Serial.println("");
    Serial.print("previous P: "); Serial.print(solar_data->p_prev); Serial.print(" mW");
    Serial.println("");
    Serial.print("current  D: "); Serial.print(pwm_panel_data->d_cycle); Serial.print(" % ");
    Serial.println("");
    Serial.print("previous D: "); Serial.print(pwm_panel_data->d_prev); Serial.print(" % ");
    Serial.println("");
    Serial.print("timer : "); Serial.println(OCR2B);
    Serial.println("");
  }
  #endif
  
 //now update the solar_data for the next cycle 
  solar_data->p_prev    = solar_data->power;
  solar_data->v_prev    = solar_data->voltage;
  pwm_panel_data->d_prev  = pwm_panel_data->d_cycle;
}

void sweeping(pwm_data* pwm_panel_data)
{
  if (pwm_panel_data->d_cycle > 6)
  {
    increase_voltage(pwm_panel_data->d_cycle);
  }
  else
  {
    turn_off_pwm();
  }
}

/*
 * @brief   This is the main program loop. 
 *
 */
void loop(void) 
{
  static mppt_data    solar_data      = {0, 0, 0, 0, 0};
  static pwm_data     pwm_panel_data  = {95, 0};
  static arduino_state  state         = state_start_up;

  // Usage of switch case statement allows easy modification of program flows
  switch (state)
  {
    default:
    case state_start_up: 
      {
        delay(10000);
        turn_on_pwm();
        set_duty_cycle(pwm_panel_data.d_cycle);
        state = state_get_power;
        break;
      }

    case state_get_power:
      {
        getPower(&solar_data);
        state = state_tracking;
        break;
      }

    case state_tracking:
      {  // true if MPP is reached, else false
        PandO(&solar_data, &pwm_panel_data);
        //sweeping(&pwm_panel_data);
        print_data(solar_data, pwm_panel_data);
        state = state_get_power;
        delay(10);
        break;
      }
    
	  case state_sleep:
	   {
	     // check if it's time to wake up and recheck MPP
  	   static unsigned long previous_time = 0; 
  	   unsigned long current_time = millis();
        
  	   if (current_time - previous_time >= SLEEP_TIME) 
  	   {
  	     previous_time = current_time;
  	     state = state_get_power;
  	   }
  	   break;
	   }
  } // end switch 
} // end loop 


