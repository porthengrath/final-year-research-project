#include <Wire.h>
#include <Adafruit_INA219.h>

// instantiate adafruit power sensing unit
Adafruit_INA219 ina219;


// we need to set up the output pin for the PWM signal 
static const uint8_t PWM_PIN = PD3;   

static const double K_MPP1 = 0.76;   // Our constant Kv value from (Vmpp = Kv x Voc)
// static const double K_MPP2 =0.82;

// D = sqrt(0.9* Rout * I_mpp/V_mpp) @90% efficiency     
static const uint8_t INITIAL_D_1PANEL = 47; 
static const uint8_t INITIAL_D_2PANEL = 33;
static const uint8_t INITIAL_D_3PANEL = 27;

// // stop MPP tracking if current and previous power difference are only this much
// static const double P_MPP_HYSTERESIS = 0.8;   // 0.8 mWatts from INA219 power bit-resolution

// static const double P_TRACK_HYSTERESIS = 0.1; // 0.1 mWatts

// static const double V_TRACK_HYSTERESIS = 0.01;   // 0.1 V

// minimum allowable step size due to timer resolution 1/160 * 100 = 0.625 
static const double STEP_SIZE = 0.625;	

// sleep time 10 000 ms
static const unsigned long SLEEP_TIME = 25000; 

struct mppt_data
{
  float current_mA; 
  float voltage; 
  float power; 
  float voltage_vcm;
  
  float v_prev;
  float p_prev;    
};


// duty cycle measured in percentage
struct pwm_data
{
  double d_cycle;     // limit between 10 and 90%
  double d_prev;      // previous duty cycle history
};

enum arduino_state 
{
  state_start_up,
  state_get_power,
  state_tracking,
  state_sleep,   // sleep, doesn't exist for P&O
};

/*
 *	@brief non-blocking timeout, use to perform non-blocking wait
 *
 *	@return bool, return true is timer expired. 
 *
 */
bool timeout()
{
  static unsigned long current_time = 0;
  static unsigned long last_time = 0;

  current_time = millis();
  if (current_time - last_time >= 20000) // 2 sec period
  {
    last_time = current_time;
    return true;
  }
  else
  {
   return false; 
  }
}

void setup_timer_2_pwm() 
{
    DDRD |= (1 << PWM_PIN);          // set PD3 (OC2B) as output
    OCR2A = 159;                 // set period as 160 ticks (100kHz @ 1 prescaler, 16 MHz clk)
    OCR2B = 79;                  // set initial dutycycle as 80 ticks (50% at 160 ticks period)
    
    TCCR2A = 0;                   // clear TCCR2A register
    TCCR2B = 0;                   // clear TCCR2B register
    TCCR2A |= (1 << COM2B1);      // set timer Ch B as non-inverting PWM mode (Clear on Capture)
    
    // set timer as Fast PWM mode with OCR2A as TOP (period)
    TCCR2A |= (1 << WGM21)|(1 << WGM20);  
    TCCR2B |= (1 << WGM22);
    
    TCCR2B |= (1 << CS10);        // use 1 prescaler and enable timer
}

void turn_off_pwm() 
{  
  TCCR2B &= ~(1 << CS10); 
  //digitalWrite(PD3, LOW);
}

void turn_on_pwm()  {  TCCR2B |= (1 << CS10); }

/*
 *  @brief set the duty cycle of PWM frequency. Ranges from
 *   
 */
 
void set_duty_cycle(double d_cycle_percent)
{
    double duty_percent = d_cycle_percent;

    // map percentage values to timer tick values
    uint8_t duty_cycle_ticks = (duty_percent * (OCR2A + 1))/100;

    // change the PWM duty cycle
    OCR2B = duty_cycle_ticks;  
}

void setup(void) 
{
  uint32_t    currentFrequency;
  
  setup_timer_2_pwm();
  Serial.begin(115200);
  while (!Serial) {
      // will pause Zero, Leonardo, etc until serial console opens
      delay(1);
  }
  
  // Initialize the INA219. Range (32V, 1A)
  // Current Resolution: 1 LSB = 0.04 mA
  // Power Resolution: 1 LSB = 20 * 0.04 mA = 0.8 mW 
  ina219.begin();
  ina219.setCalibration_32V_1A();
  
  // enable all interrupts
  sei();  
}


/*
 *	@brief print data in comma seperated value(csv) format so excel can open easily  
 *	
 * 	@param solar_data		solar panels measured voltage, current, and power to print to console
 *  @param pwm_panel_data 	pwm duty cycles
 * s
 */
void print_data(mppt_data	solar_data, pwm_data pwm_panel_data)
{	
	static bool title_printed = false;
	
	if (!title_printed)
	{
		Serial.print("time (ms)");
		Serial.print(",");
		Serial.print("Voltage (v)");
		Serial.print(",");
    Serial.print("Voltage_cvm (v)");
    Serial.print(",");
		Serial.print("Current (mA)");
		Serial.print(",");
		Serial.print("Power (mW)");
		Serial.print(",");
		Serial.print("Duty cycle (percent)");
		Serial.print("\n");
		title_printed = true;
  	}
  	else 
  	{
  		Serial.print(millis());
  		Serial.print(",");
  		Serial.print(solar_data.voltage);
  		Serial.print(",");
      Serial.print(solar_data.voltage_vcm);
      Serial.print(",");
  		Serial.print(solar_data.current_mA);
  		Serial.print(",");
  		Serial.print(solar_data.power);
  		Serial.print(",");
  		Serial.print(pwm_panel_data.d_cycle);
  		Serial.print("\n");
  	}
}

/*
 *  @brief increase voltage, decrease duty cycle
 *
 *	@param double* duty_cycle_percent, 10 - 90
 *
 */
void increase_voltage(double 	&duty_cycle_percent)
{
	if (duty_cycle_percent < 5)
	{
		duty_cycle_percent = 5;
	}
	else
	{
		duty_cycle_percent -= STEP_SIZE;
	}
	set_duty_cycle(duty_cycle_percent);
}

/*
 *  @brief decrease voltage, increase duty cycle
 *
 *	@param double* duty_cycle_percent, 10 - 90
 *
 */
void decrease_voltage(double		&duty_cycle_percent)
{
	if (duty_cycle_percent > 95)
	{
		duty_cycle_percent = 95;
	}
	else
	{
		duty_cycle_percent += STEP_SIZE;
	}
	set_duty_cycle(duty_cycle_percent);
}

/*
 * @brief get the voltage, current reading from INA219 high-side monitoring
 *	      All measurement are sampled a certain number of times to prevent transient
 *		  effect of perturbing duty cycle from affecting algorithm. 
 * 
 * @param mppt_data,  solar panels voltage, current, and power measurements to collect 
 *
 */
void getPower(mppt_data*    solar_data)
{  
  static const uint8_t SAMPLE_AVERAGE = 50;

  double current_mA = 0;
  double load_voltage = 0;
  double power_mW = 0;

  for (int i = 0; i < SAMPLE_AVERAGE; i++ )
  {
    current_mA += ina219.getCurrent_mA();
    power_mW += ina219.getPower_mW();
    load_voltage += ina219.getBusVoltage_V() + (ina219.getShuntVoltage_mV()/1000);
  }
  
  current_mA /= SAMPLE_AVERAGE;
  power_mW /= SAMPLE_AVERAGE;
  load_voltage /= SAMPLE_AVERAGE;
  
  solar_data->voltage     = load_voltage;
  solar_data->current_mA  = current_mA;
  solar_data->power       = power_mW;
}

/*
 * @brief   Fractional Open-Circuit Voltage MPP method. Using solar panel characteristic of 
 *			Vmpp = constant * Voc, we can do a quick and cheap way of tracking power point.
 *			Also, using the fact that between Voc and Vmpp, the increase in duty cycle is almost 
 *			proportional to the decrease in voltage, we can reached MPP quicker. 
 *			This code is inspired by Adam Pravinski. 
 *
 * @param 	mppt_data*  pointer to solar_data voltage, power data struct which is to be updated 
 *      	pwm_data* pointer to pwm_data dutycycle struct which is to be updated
 *
 * @return  true, when solar panel voltage is close to Vcvm, else false.
 *			
 */
bool FOCV (mppt_data* solar_data, pwm_data* pwm_panel_data)
{
  // make them static so they don't lose value every function call
  static double Vcvm = 0;
  static bool is_startup = true;
  double step_size = 0;

  // do it once every n seconds or start up condition
  if ((timeout()) || (is_startup == true))
  {
  	set_duty_cycle(1);              // sometimes turning off timer when pwm is high causes weird thing to happen
    delay(10);
  	getPower(solar_data);						 // get open circuit voltage
  	//set_duty_cycle(pwm_panel_data->d_cycle);	 // set to 5% dutycycle, from high voltage to low
  	Vcvm = K_MPP1 * solar_data->voltage;
    solar_data->voltage_vcm = Vcvm;
  	is_startup = false;
  }	

  if ((solar_data->voltage - Vcvm > 0.1) || (Vcvm - solar_data->voltage > 0.1))
	{
	  // a 0.625 % changes in dutycycle cause roughly 0.1 V changes in panel voltage from graph
	  // we can approximate the require step-size using the formula below. 
	  // stepsize could be positive or negative
	  step_size = (solar_data->voltage - Vcvm) * 0.625/0.2;	
	  
//	  if (step_size > 0 && step_size < 0.625)
//	  {
//	 	step_size = 0.625; 	
//	  }
    
	  pwm_panel_data->d_cycle += step_size;
	  if (pwm_panel_data->d_cycle > 95)
	  {
	  	pwm_panel_data->d_cycle = 95;
	  }
	  else if (pwm_panel_data->d_cycle < 5)
	  {
	  	pwm_panel_data->d_cycle = 5;	
	  }
	  set_duty_cycle(pwm_panel_data->d_cycle);
	  return false;
	}
  else
  {
  	  // we have reached MPP
  	  return true;
  } 
}
 

/*
 * @brief   This is the main program loop. 
 *
 */
void loop(void) 
{
  static mppt_data    solar_data      = {0, 0, 0, 0, 0};
  static pwm_data     pwm_panel_data  = {5, 0};
  static arduino_state  state         = state_start_up;

  // Usage of switch case statement allows easy modification of program flows
  switch (state)
  {
    default:
    case state_start_up: 
      {	
      	// focv starting condition is a little bit different from other
        //turn_off_pwm();
        //set_duty_cycle(pwm_panel_data.d_cycle);
        delay(5000);
        state = state_get_power;
        break;
      }

    case state_get_power:
      {
        getPower(&solar_data);
        state = state_tracking;
        break;
      }

    case state_tracking:
      {  // true if MPP is reached, else false
        if (!FOCV(&solar_data, &pwm_panel_data))
        {
        	state = state_get_power;
        	print_data(solar_data, pwm_panel_data);
        	delay(10);
    	}
    	else
    	{
    		state = state_sleep;
    		print_data(solar_data, pwm_panel_data);
    	}
        break;
      }
    
	case state_sleep:
	  {  // check if it's time to wake up and recheck MPP
		  unsigned long current_time = millis();
      static unsigned long previous_time = 0;
      static unsigned long previous_time_2 = 0;
		  if (current_time - previous_time >= SLEEP_TIME) 
		  {
		    previous_time = current_time;
		    state = state_get_power;
		  }
      else if (current_time - previous_time_2 >= 1000)
      {
        previous_time_2 = current_time;
        print_data(solar_data, pwm_panel_data);
      }
		  break;
	  }
	}
}
