#ifndef ACO_H_INCLUDED
#define ACO_H_INCLUDED

class Ant
{
    public:

        //number of ants
        int num_ants;
        //number of new ants
        int new_ant;
        float newDuty [4];
        float updated_power[4];
        //max number of iterations
        int Maxit;
        float best_power_val [4];
        float best_duty_val [4];
        float power[4];
        float dutyCycle [4];
        //reference value
        float position_ant [4];
        float duty_position[4];

        float Watts;
        double t;

        void setting_Duty_ants ();

        //this function will update the best possible solution in to the solution archive
        void start_initial_ant_population();
        void update_Sol (float *duty,float *power);
        void updating_duty_cycle();
        void updating_power();
};

#endif // ACO_H_INCLUDED
