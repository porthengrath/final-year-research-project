/*
 * pso.h
 *
 *  Created on: 16 May 2019
 *      Author: Portheng
 */
#include <stdint.h>
#include "pwm.h"
#include "Adafruit_INA219.h"

#ifndef PSO_H_
#define PSO_H_

struct particle
{
    float duty_cycle;
    float velocity;
    float voltage;
    float current;
    float power;
    float local_best_power;
    float local_best_duty_cycle;
};

class pso
{
    enum state
    {
        state_initialise,
        state_measure_v_p,
        state_update_pso,
        state_sleep,
        state_reinit,
    };

private:
    static const uint8_t    PARTICLE_NUM = 3;
    static const uint8_t    MAX_ITERATION = 50;
    static const uint8_t    MPP_POWER_DIFFERENCE = 20;  // 20 mW power difference

    float                   inertia;

    float                  global_best_power;
    float                  global_best_current;
    float                  global_best_voltage;
    float                  global_best_duty_cycle;

    uint8_t                 iteration_count;
    uint8_t                 convergence_count;
    particle                pso_particle[PARTICLE_NUM];

    pwm*                    pwm_timer_2;
    Adafruit_INA219*        ina219;
    state                   pso_state;

    /*
     * reset global and local best values and iteration counters
     */
    void                    initialise();

    void                    print_data();
    void                    get_voltage_power(int particle_number);

public:
                            pso(pwm*                    pwm_timer,
                                Adafruit_INA219*        ada_ina219);
    void                    run_state_machine();
};


#endif /* PSO_H_ */
