
%% Problem Definition

CostFunction=@ Sphere;        % Cost Function

nVar=2;             % Number of Decision Variables

VarSize=[1 nVar];   % Variables Matrix Size

VarMin= -10;        % Decision Variables Lower Bound
VarMax= 10;         % Decision Variables Upper Bound

VarMin2 = -10; 
VarMax2 = 10; 

 
%% ACOR Parameters

MaxIt=500;          % Maximum Number of Iterations

nPop=10;            % Population Size (Archive Size)

newPop=50;          % New population 

q=0.5;              % Intensification Factor (Selection Pressure)

zeta=1;             % Deviation-Distance Ratio

t = 0.05;           % Standard deviation

numSol = 10;        % Number of solution to the archive  

Vref = 5;           % Reference voltage  

C = 0;              % Counter 

MaxPower = 100;     % Maximum possible power from the pannels 

start = 0;          % Start value, for the new population 
%% Initialization

% Create Empty Individual Structure
empty_individual.Position=[];
empty_individual.Cost=[];

% Create Population Matrix
pop=repmat(empty_individual,nPop,1);

% Initialize Population Members
for i=1:nPop
    
    % Create Random Solution
    pop(i).Position(1)=unifrnd(VarMin,VarMax);
    pop(i).Position(2)=unifrnd(VarMin2,VarMax2);
    
    % Evaluation
    pop(i).Cost= CostFunction(pop(i).Position(1),pop(i).Position(2));
    
end

% Sort Population
[~, SortOrder]=sort([pop.Cost]);
pop=pop(SortOrder);

%Now we have to select a reference point (for best value) 
Ant_ref = pop(1); 

% Array to hold Best Cost values 
BestCost = zeros (MaxIt,1); 

% we have to create a soultion archive to put voltage and power
Solution_archive = zeros (numSol,3); 

% Now lets put the best solution in to the Solution_archive

for V=1:numSol   
         Solution_archive(V) = pop(V).Position(1);  
end 

for x=1:numSol
     Solution_archive(x,2) = pop(x).Cost;    
end 

for x=1:numSol
     Solution_archive(x,3) = pop(x).Position(2);    
end 

%% ACOR Main Loop

for it=1:MaxIt
   
    %Array that holds the distance between the best soultion and selected
    %position (4)
    Distance = zeros (numSol,nVar);   
    for l=1:numSol
        Distance(l)= abs (Solution_archive(l)- Solution_archive(1) );
        Distance(l,2)= abs (Solution_archive(l,3)- Solution_archive(1,3) );
        %Distance(l,1)= abs (pop(l).Position(1) - pop(1).Position(1));
        %Distance(l,2)= abs (pop(l).Position(2) - pop(1).Position(2));
    end
    
    %Array that holds the Guassian equation (5)
    Guassian = zeros (numSol,nVar); 
    
    for l=1:numSol
        Guassian(l) = exp (-(Distance(l)^2)/ (2*t));
        Guassian(l,2) = exp (-(Distance(l,2)^2)/ (2*t));
    end
 
    %The sum of the Guassian values 
    S_one = sum(Guassian(:,1)); 
    S_two = sum(Guassian(:,2)); 
  
    %Array that holds the pheromone values for each solution 
    Pheromone = zeros (numSol,1);
    
    for l=1:numSol
        Pheromone(l) = Guassian(l) /S_one; 
        Pheromone(l,2) = Guassian(l,2) /S_two; 
    end
   
    
    % A matrix that hold all the information about the refernce point
    % and number of ants at each reference point 
     Reference_Data = zeros(numSol,nVar); 
    
    % Need to select the refernce point based on the Pheromone levels
    for l=1:numSol
    % Updating the number of ants for each position 
        Reference_Data(l) = fix(newPop*Pheromone(l)); 
        Reference_Data(l,2) = Solution_archive(l); 
        Reference_Data(l,3) = Solution_archive(l,3);         
    end  

    % Create New Population Array
    newAnt=repmat(empty_individual,newPop,1);
   
    
    % Now we need to give positions to the new ant population 
    for l=1:numSol
        
        % Number of new ants for each selected position 
        num_New_Ants = Reference_Data(l)+ start; 
        
        
        disp(['number of new ants:  ' num2str(num_New_Ants)]);
        disp(['start value:   ' num2str(start)]);
         
       for i=start+1:num_New_Ants
          newAnt(i).Position(1) = Reference_Data(l,2) + unifrnd (-2,2);
          newAnt(i).Position(2) = Reference_Data(l,3) + unifrnd (-2,2);
          newAnt(i).Cost=CostFunction(newAnt(i).Position(1),newAnt(i).Position(2)); 
       end
       % Now we need to update the starting value 
       start = fix (Reference_Data(l)) + start; 
    end    
       
%     for t=1:newPop
%         
%         % Initialize Position Matrix
%         newAnt(t).Position=zeros(VarSize);
%         
%         % Solution Construction
%         newAnt(t).Position(1) = Ant_ref.Position(1) + unifrnd (-0.5,0.2);  
%         newAnt(t).Position(2) = Ant_ref.Position(2) + unifrnd (-0.5,0.2);  
%         
%         if newAnt(t).Position(1)< VarMax && newAnt(t).Position(2)< VarMax2
%          % Evaluation
%          disp(['new position x:  ' num2str(newAnt(t).Position(1))]);
%          newAnt(t).Cost=CostFunction(newAnt(t).Position(1),newAnt(t).Position(2));
%         else
%          newAnt(t).Cost=CostFunction(VarMax,VarMax2);
%          disp(['Maximum power reached!!!:  ' num2str(newAnt(t).Cost)]);
%         end
%         
%     end
    
    % Merge Main Population (Archive) and New Population (Samples)
    pop=[pop
         newAnt]; %#ok
     
    % Sort Population
    [~, SortOrder]=sort([pop.Cost]);
    pop=pop(SortOrder);
    
    % Delete Extra Members
    pop=pop(1:nPop);
    
    % Update Best Solution Ever Found
    Ant_ref=pop(1);
   
    % Store Best Cost
    %BestCost(it)=Ant_ref.Cost;
    
    % Update the Solution_archive
    for V=1:numSol   
         Solution_archive(V) = pop(V).Position(1);  
    end 

    for x=1:numSol
     Solution_archive(x,2) = newAnt(x).Cost;    
    end 

    for x=1:numSol
     Solution_archive(x,3) = newAnt(x).Position(2);    
    end 
    
    % Also its important to make sure to put Start value back to zero
    start = 0; 
    
    % Show Iteration Information
    disp(['X and Y:  ' num2str(Ant_ref.Position)]);
    disp(['Best Cost = ' num2str(Ant_ref.Cost)]);
    
    C = C +1; 
    %disp(['Counter (C) is:  ' num2str(C)]);
        
    if (C == MaxIt)
    % After the Maximum number of iterations update the duty cycle
    D = Vref / Ant_ref.Position(1); 
    disp(['Duty Cycle is:  ' num2str(D)]);
    end

end
%}
%% Results
% 
% figure;
% %plot(BestCost,'LineWidth',2);
% semilogy(BestCost,'LineWidth',2);
% xlabel('Iteration');
% ylabel('Best Cost');
% grid on;
