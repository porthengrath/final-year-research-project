/*
 * pwm.cpp
 *
 *  Created on: 16 May 2019
 *      Author: Portheng Rath
 */
#include "pwm.h"


pwm::pwm()
{
}

void pwm::setup_pwm()
{
    DDRD |= (1 << PWM_PIN);          // set PD3 (OC2B) as output
    OCR2A = 159;                 // set period as 160 ticks (100kHz @ 1 prescaler, 16 MHz clk)
    OCR2B = 79;                  // set initial dutycycle as 80 ticks (50% at 160 ticks period)

    TCCR2A = 0;                   // clear TCCR2A register
    TCCR2B = 0;                   // clear TCCR2B register
    TCCR2A |= (1 << COM2B1);      // set timer Ch B as non-inverting PWM mode (Clear on Capture)

    // set timer as Fast PWM mode with OCR2A as TOP (period)
    TCCR2A |= (1 << WGM21)|(1 << WGM20);
    TCCR2B |= (1 << WGM22);

    TCCR2B |= (1 << CS10);        // use 1 prescaler and enable timer
}

void pwm::turn_on_timer()
{
    TCCR2B |= (1 << CS10);
}

void pwm::turn_off_timer()
{
    TCCR2B &= ~(1 << CS10);
}

void pwm::set_duty_cycle(double d_cycle_percent)
{
    double duty_percent = d_cycle_percent;
    if (duty_percent < 5)
    {
        duty_percent = 5;
    }
    else if (duty_percent > 95)
    {
        duty_percent = 95;
    }
    // map percentage values to timer tick values
    uint8_t duty_cycle_ticks = (duty_percent * (OCR2A + 1))/100;

    // change the PWM duty cycle
    OCR2B = duty_cycle_ticks;
}



