################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Adafruit_INA219.cpp \
../main.cpp \
../pso.cpp \
../pwm.cpp 

OBJS += \
./Adafruit_INA219.o \
./main.o \
./pso.o \
./pwm.o 

CPP_DEPS += \
./Adafruit_INA219.d \
./main.d \
./pso.d \
./pwm.d 


# Each subdirectory must supply rules for building sources it contributes
Adafruit_INA219.o: ../Adafruit_INA219.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\arduino" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\standard" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\libraries" -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"Adafruit_INA219.d" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\arduino" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\standard" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\libraries" -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


