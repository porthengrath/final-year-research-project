/*
 * if extern statement is declared in header file. need to be include and compile with project files. Not as <>
 * but as " "
 */

#include <Wire.h>
#include "Adafruit_INA219.h"
#include "Arduino.h"
#include "pwm.h"
#include "pso.h"

// instantiate adafruit power sensing unit
Adafruit_INA219 ina219;
pwm             pwm_timer_2;
pso             l_pso(&pwm_timer_2, &ina219);

/*
 *  @brief non-blocking timeout, use to perform non-blocking wait
 *
 *  @return bool, return true is timer expired.
 *
 */
bool timeout()
{
  static unsigned long current_time = 0;
  static unsigned long last_time = 0;

  current_time = millis();
  if (current_time - last_time >= 2000) // 1500 msec period
  {
    last_time = current_time;
    return true;
  }
  else
  {
   return false;
  }
}

void setup(void)
{
  Serial.begin(115200);
  pwm_timer_2.setup_pwm();
  pwm_timer_2.turn_on_timer();

  while (!Serial) {
      // will pause Zero, Leonardo, etc until serial console opens
      delay(1);
  }

  // Initialize the INA219. Range (32V, 1A)
  // Current Resolution: 1 LSB = 0.04 mA
  // Power Resolution: 1 LSB = 20 * 0.04 mA = 0.8 mW
  ina219.begin();
  ina219.setCalibration_32V_1A();

  // enable all interrupts
  sei();
}


/*
 * @brief   This is the main program loop.
 *
 */
void loop(void)
{
    l_pso.run_state_machine();
}

int main()
{
    init();
    setup();
    delay(4000);
    while(1)
    {
        loop();
    }
    return 0;
}

