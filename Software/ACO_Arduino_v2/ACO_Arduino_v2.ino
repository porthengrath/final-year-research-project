#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_INA219.h>
#include <stdlib.h>
#include "acoduty.h"

// instantiate adafruit power sensing unit
Adafruit_INA219 ina219;

//create an object
Ant           ants;

// we need to set up the output pin for the PWM signal
static const uint8_t PWM_PIN = PD3;


enum States
{
  Start_ACO,
  main_ACO_loop
};

void setup_timer_2_pwm()
{
    DDRD |= (1 << PWM_PIN);          // set PD3 (OC2B) as output
    OCR2A = 159;                 // set period as 160 ticks (100kHz @ 1 prescaler, 16 MHz clk)
    OCR2B = 79;                  // set initial dutycycle as 80 ticks (50% at 160 ticks period)

    TCCR2A = 0;                   // clear TCCR2A register
    TCCR2B = 0;                   // clear TCCR2B register
    TCCR2A |= (1 << COM2B1);      // set timer Ch B as non-inverting PWM mode (Clear on Capture)

    // set timer as Fast PWM mode with OCR2A as TOP (period)
    TCCR2A |= (1 << WGM21)|(1 << WGM20);
    TCCR2B |= (1 << WGM22);

    TCCR2B |= (1 << CS10);        // use 1 prescaler and enable timer
}

void turn_off_pwm() {  TCCR2B &= ~(1 << CS10); }
void turn_on_pwm()  {  TCCR2B |= (1 << CS10); }

//this function is responsible for initialise the values
void Initialise (Ant *ants)
{
    ants->num_ants = 4;
    ants->Maxit = 1;
    ants->t = 0.05;
    ants->new_ant = 4;
}

void setup(void)
{
    uint32_t    currentFrequency;

    setup_timer_2_pwm();
    Serial.begin(115200);
    while (!Serial)
    {
      // will pause Zero, Leonardo, etc until serial console opens
      delay(1);
    }

    // Initialize the INA219. Range (32V, 1A)
    // Current Resolution: 1 LSB = 0.04 mA
    // Power Resolution: 1 LSB = 20 * 0.04 mA = 0.8 mW
    ina219.begin();
    ina219.setCalibration_32V_1A();
    // enable all interrupts
    sei();
}



/*
 *  @brief set the duty cycle of PWM frequency. Ranges from
 *
 */
void set_duty_cycle(double d_cycle_percent)
{
    double duty_percent = d_cycle_percent;

    // map percentage values to timer tick values
    uint8_t duty_cycle_ticks = (duty_percent * (OCR2A + 1));

    // change the PWM duty cycle
    OCR2B = duty_cycle_ticks;
}


//this function is responsible for taking the current power from the solar pannel
void getPower(Ant *ants, int number)
{
  static const uint8_t SAMPLE_AVERAGE = 20;

  double current_mA = 0;
  double load_voltage = 0;
  double power_mW = 0;

  //unsigned long last_time = millis();
  for (int i = 0; i < SAMPLE_AVERAGE; i++ )
  {
    current_mA += ina219.getCurrent_mA();
    power_mW += ina219.getPower_mW();
    load_voltage += ina219.getBusVoltage_V() + (ina219.getShuntVoltage_mV()/1000);
  }
  //unsigned long elapsed_time = millis() - last_time;
  //Serial.print("time taken to read power: "); Serial.print(elapsed_time); Serial.println("");

  current_mA /= SAMPLE_AVERAGE;
  power_mW /= SAMPLE_AVERAGE;
  load_voltage /= SAMPLE_AVERAGE;

  //now we need to add the updated power to the array
  ants->power[number] = power_mW;
}


void printValues (Ant *ants,int counter,int function,int numIT)
{
  if (function == 0)
  {
        //printing the best updated solutions in the solution archive
      Serial.print("the preset duty cycle is: ");
      Serial.println (ants->best_duty_val[counter]);
      //printing the new updated power
      Serial.print("the power from solar PV is: ");
      Serial.println(ants->best_power_val[counter]);
  }
  else if (function == 1)
  {
      //printing the best updated solutions in the solution archive
      Serial.print("the updated duty cycle is: ");
      Serial.println (ants->newDuty[counter]);
      //printing the new updated power
      Serial.print("the new updated power is: ");
      Serial.println(ants->power[counter]);
      Serial.print("And the number of iteration: ");
      Serial.println();
  }
  else if (function ==2)
  {
      Serial.print("After the iteration the best possible power is: ");
      Serial.println(ants->best_power_val[0]);
      Serial.print("And the Duty cycle: ");
      Serial.println(ants->best_duty_val[0]);
  }
}

void loop()
{

     static States state = Start_ACO;

     switch (state)
     {
          case (Start_ACO):
          {
                Serial.println("now starting!!!");
                 //now initialise the object
                 Initialise(&ants);
                 turn_on_pwm();
                 //now we are setting the initial duty cycles of the ants
                 Serial.println("setting the initial duty cycles ");
                 ants.setting_Duty_ants();
                 //next step is to calculate the power for each preset duty cycle for each ant
                 for (int i = 0; i<ants.num_ants; i++)
                 {
                      set_duty_cycle(ants.dutyCycle[i]);
                      //adding a 10ms delay
                      delay(10);
                      //now  get power for each different duty cycle
                      getPower(&ants,i);
                      //printing the values
                      printValues(&ants,i,0,0);

                 }
                 //now we can start the initial ant population
                 ants.start_initial_ant_population();

                 //now each ant should have pre set duty cycle and the associated power
                 //change the state
                 state = main_ACO_loop;
                 break;
          }

          case (main_ACO_loop):
              {
                  Serial.print("state main ACO loop");
                  for (int it = 1; it<=ants.Maxit; it++)
                  {
                      //now we are going to find the best possible duty cycle
                      ants.updating_duty_cycle();
                      //next step is to find the power associated with the best possible duty cyles
                      for (int i = 0; i<ants.num_ants; i++)
                      {
                          //cout<<"the best updated duty_cycle: "<<ants.best_duty_val[i]<<endl;
                          set_duty_cycle(ants.newDuty[i]);

                          //adding a 10ms delay
                          delay(10);

                          //now get the power for each set duty cycle
                          //getPower_from_csv(&ants,i);
                          getPower(&ants,i);
                          //printing the values
                          printValues(&ants,i,1,it);
                      }
                      //once the solution archive is filled with power and the associated duty cycle we can update it.
                      ants.updating_power();
                      printValues(&ants,0,2,0);
                  }
                  //after the iteration start again
                  state = Start_ACO;
                  break;
              }

     }

}

