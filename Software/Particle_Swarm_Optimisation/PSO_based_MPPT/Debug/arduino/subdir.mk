################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../arduino/WInterrupts.c \
../arduino/hooks.c \
../arduino/wiring.c \
../arduino/wiring_analog.c \
../arduino/wiring_digital.c \
../arduino/wiring_pulse.c \
../arduino/wiring_shift.c 

CPP_SRCS += \
../arduino/CDC.cpp \
../arduino/HardwareSerial.cpp \
../arduino/HardwareSerial0.cpp \
../arduino/HardwareSerial1.cpp \
../arduino/HardwareSerial2.cpp \
../arduino/HardwareSerial3.cpp \
../arduino/IPAddress.cpp \
../arduino/PluggableUSB.cpp \
../arduino/Print.cpp \
../arduino/Stream.cpp \
../arduino/Tone.cpp \
../arduino/USBCore.cpp \
../arduino/WMath.cpp \
../arduino/WString.cpp \
../arduino/abi.cpp \
../arduino/new.cpp 

S_UPPER_SRCS += \
../arduino/wiring_pulse.S 

C_DEPS += \
./arduino/WInterrupts.d \
./arduino/hooks.d \
./arduino/wiring.d \
./arduino/wiring_analog.d \
./arduino/wiring_digital.d \
./arduino/wiring_pulse.d \
./arduino/wiring_shift.d 

OBJS += \
./arduino/CDC.o \
./arduino/HardwareSerial.o \
./arduino/HardwareSerial0.o \
./arduino/HardwareSerial1.o \
./arduino/HardwareSerial2.o \
./arduino/HardwareSerial3.o \
./arduino/IPAddress.o \
./arduino/PluggableUSB.o \
./arduino/Print.o \
./arduino/Stream.o \
./arduino/Tone.o \
./arduino/USBCore.o \
./arduino/WInterrupts.o \
./arduino/WMath.o \
./arduino/WString.o \
./arduino/abi.o \
./arduino/hooks.o \
./arduino/new.o \
./arduino/wiring.o \
./arduino/wiring_analog.o \
./arduino/wiring_digital.o \
./arduino/wiring_pulse.o \
./arduino/wiring_shift.o 

S_UPPER_DEPS += \
./arduino/wiring_pulse.d 

CPP_DEPS += \
./arduino/CDC.d \
./arduino/HardwareSerial.d \
./arduino/HardwareSerial0.d \
./arduino/HardwareSerial1.d \
./arduino/HardwareSerial2.d \
./arduino/HardwareSerial3.d \
./arduino/IPAddress.d \
./arduino/PluggableUSB.d \
./arduino/Print.d \
./arduino/Stream.d \
./arduino/Tone.d \
./arduino/USBCore.d \
./arduino/WMath.d \
./arduino/WString.d \
./arduino/abi.d \
./arduino/new.d 


# Each subdirectory must supply rules for building sources it contributes
arduino/%.o: ../arduino/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\arduino" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\standard" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\libraries" -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

arduino/%.o: ../arduino/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\arduino" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\standard" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\libraries" -DARDUINO=328 -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

arduino/%.o: ../arduino/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Assembler'
	avr-gcc -x assembler-with-cpp -I"C:/Program Files (x86)/Arduino/hardware/arduino/avr/libraries" -I"C:/Program Files (x86)/Arduino/hardware/arduino/avr/libraries/Wire/src" -I"C:\Users\Portheng\Documents\fyp2\Software\Particle_Swarm_Optimisation\PSO_based_MPPT\libraries" -g2 -gstabs -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


