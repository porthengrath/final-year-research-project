// Funny thing happens when we use timer 0 since the Arduino library uses it for other purposes.
// best to avoid changing timer 0 prescaler or period.

static volatile uint16_t period_in_ticks = 0;
static volatile uint16_t dutycycle_in_ticks = 0;

// input capture interrupt to measure frequency and duty cycle, 
// since we are broke and don't have an oscilloscope. 
ISR(TIMER1_CAPT_vect) 
{
  static uint16_t current_edge = 0;
  static uint16_t last_edge = 0;
  static uint16_t stop_edge = 0;
  // first edge should be rising edge
  current_edge = ICR1;
  uint16_t tccr1b = TCCR1B;
  TCCR1B = tccr1b ^ (1 << ICES1);
  if ((tccr1b & (1 << ICES1)) == (1 << ICES1))     // rising edge
  {
    period_in_ticks = current_edge - last_edge;
    dutycycle_in_ticks = stop_edge - last_edge;
    last_edge = current_edge;
  }
  else
  {
    stop_edge = current_edge;
  }
}

void setup_timer_1_capture()
{
  DDRB &= (1 << PB0);                   // use ICP1 as input capture pin
  TCCR1A = 0;                           // clear registers
  TCCR1B = 0;
  TCCR1B |= (1 << ICES1)|(1 << CS10);   // use rising edge trigger
  TIFR1 = 1 << ICF1;                      // clear irq flag
  TIMSK1 |= (1 << ICIE1);               // turn on input capture interrupt
}

// obselete, can use timer 1 to generate PWM but only timer 1 has input capture
void setup_timer_1_pwm() 
{
    DDRB |= (1 << PB1);           // set PB1 as output
    ICR1 = 0x9F;                  // set period as 160 ticks (100kHz @ 1 prescaler)
    OCR1A = 0x49;                 // set dutycycle as 16 ticks (10% at 160 ticks period)
    TCCR1A  = 0;              // clear TCCR1A register
    TCCR1B  = 0;
    TCCR1A |= (1 << COM1A1);      // set timer as non-inverting PWM mode (Clear on Capture)
    
    // set timer as Fast PWM mode with ICR1 as TOP (period)
    TCCR1A |= (1 << WGM11)|(0 << WGM10);  
    TCCR1B |= (1 << WGM12)|(1 << WGM13);   
    
    TCCR1B |= (1 << CS10);        // use 1 prescaler and enable timer
}

void setup_timer_2_pwm() 
{
    DDRD |= (1 << PD3);           // set PD5 (OC0B) as output
    OCR2A = 159;                 // set period as 160 ticks (100kHz @ 1 prescaler, 16 MHz clk)
    OCR2B = 79;                  // set dutycycle as 80 ticks (50% at 160 ticks period)
    
    TCCR2A = 0;                   // clear TCCR0A register
    TCCR2B = 0;                   // clear TCCR0A register
    TCCR2A |= (1 << COM2B1);      // set timer Ch B as non-inverting PWM mode (Clear on Capture)
    
    // set timer as Fast PWM mode with OCR0A as TOP (period)
    TCCR2A |= (1 << WGM21)|(1 << WGM20);  
    TCCR2B |= (1 << WGM22);
    
    TCCR2B |= (1 << CS10);        // use 1 prescaler and enable timer
}
void setup() {
    Serial.begin(115200);
    setup_timer_2_pwm();
    setup_timer_1_capture();
    sei();
}

void loop() {
  int period = 0;
  int dutycycle = 0;
  while (1) 
  {
    cli();                          // disable interrupt
    period = period_in_ticks;
    dutycycle = dutycycle_in_ticks;
    sei();                          // enable interrupt
    Serial.print("Set d: "); Serial.print(OCR2B + 1); Serial.println("");
    Serial.print("Period is: "); Serial.print(period); Serial.println("");
    Serial.print("Duty cycle is : "); Serial.print(dutycycle); Serial.println("");
    delay(500);
  }
}
