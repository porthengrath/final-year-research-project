#include "aco.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>

double fitness_function (double voltage, double current)
{
  double power;
  power = voltage * current;
  //printf("printing the power: %f \n",power);
  return power;
}

void start_initial_population (struct ACOR_Parameters *values,struct empty_individual *InitialAnt)
{
   int i;
  // Now fill the matrix with voltage and current
  for (i= 0; i<values->number_Ants; ++i)
  {
    double volts  = values->solar_data[i][1];
    double amps = values->solar_data[i][2];
    //pop[i][1] = values->solar_data[i][1];
    InitialAnt->Position1[i] = volts;
    //pop[i][2] = values->solar_data[i][2];
    InitialAnt->Position2[i] = amps;
    //pop[i][3] = fitness_function(volts, amps);
    InitialAnt->Cost[i] = fitness_function(volts, amps);

    printf("the power is: %f =  %f *  %f \n",InitialAnt->Cost[i],InitialAnt->Position1[i],InitialAnt->Position2[i]);

  }
  //create a temp array to hold maximum value of the power
  double temp [values->number_Ants];
  double temp1 [values->number_Ants];
  double temp2 [values->number_Ants];
  //now put all zeros to it
  for (int i = 0; i<values->number_Ants; i++)
  {
      temp[i] = 0;
      temp1[i] = 0;
      temp2[i]= 0;
  }

  int array_numb;

  //now next step is to sort the struct from highest value to lowest value
  for (int i = 0; i<values->number_Ants; ++i)
  {
      for (int j =0; j<values->number_Ants; ++j)
      {
          if (temp[i] >= InitialAnt->Cost[j] )
          {
              //printf("the value is bigger than previous \n");
          }
          else if (temp[i] < InitialAnt->Cost[j])
          {
              temp[i] = InitialAnt->Cost[j];
              temp1[i] = InitialAnt->Position1[j];
              temp2[i] = InitialAnt->Position2[j];
              array_numb = j;
              //printf(" the cost %f \n", temp[i]);
          }
      }

     // next we need to remove the highest value from the array
    int length = sizeof (InitialAnt->Cost)/ sizeof (double);
    //printf("the length of the array is: %d \n", length);
    for (int x = array_numb; x < length; x++)
    {
       InitialAnt->Cost[x] = InitialAnt->Cost[x+1];
       InitialAnt->Position1[x] = InitialAnt->Position1[x+1];
       InitialAnt->Position2[x] = InitialAnt->Position2[x+1];
    }
  }

  //now that the power is arranged from min to max we can save it back to the orginal
  //array
  for (int x = 0; x<values->number_Ants; x++)
  {
      InitialAnt->Cost[x] = temp[x];
      InitialAnt->Position1[x] = temp1[x];
      InitialAnt->Position2[x] = temp2[x];
      //printf("power after sorting from max to min: %f =  %f *  %f \n",InitialAnt->Cost[x],InitialAnt->Position1[x],InitialAnt->Position2[x]);
  }
}

void Solution_archive(struct bestSolutions *topValues,
                      struct ACOR_Parameters *values,
                      struct empty_individual *InitialAnt)
{
//put the best values into this struct
    for (int x = 0; x < values->bestNum; x++)
    {
        topValues->bestWatts[x] = InitialAnt->Cost[x];
        topValues->bestVolts[x] = InitialAnt->Position1[x];
        topValues->bestAmps[x] = InitialAnt->Position2[x];
        //at the initial calculation put duty cycle as zero
        topValues->bestDutyCycle[x] = 0;
        printf("best values: %f =  %f *  %f \n",topValues->bestWatts[x],topValues->bestVolts[x],topValues->bestAmps[x]);
    }
}


// A limit function that makes sure the new voltage and current remain within the limits
//note that the values i am using are random numbers and limits since I do not posses the data
double check_Position1 (double Volt_position,int limit)
{
    //check if the random jump is on the right side or left side of the curve
    if (limit == 0)
    {
        //jump to the left side of the refernce point
        float jumpval1 = ((float)rand())/RAND_MAX * -0.1 - 2;
        printf("the left side val is: %f\n",jumpval1);
        double valueVolt = Volt_position + jumpval1;
        if (valueVolt < 12.27)
        {
            //printf("the limited voltage position is: %f \n",valueVolt);
            return valueVolt;
        }
        else
        {
            return 0;
        }
    }
    else if (limit == 1)
    {
        //jump to the right side of the refernce point
        float jumpval2 = ((float)rand())/RAND_MAX * 0.1 - 0;
        printf("the right side val is: %f\n",jumpval2);
        double valueVolt = Volt_position + jumpval2;
        if (valueVolt < 12.27)
        {
            //printf("the limited voltage position is: %f \n",valueVolt);
            return valueVolt;
        }
        else
        {
            return 0;
        }
    }
}

double check_Position2 (double Amp_position,int limit)
{
    if (limit == 0)
    {
         //jump to the left side of the refernce point
        float jumpval1 = ((float)rand())/RAND_MAX * -0.9 - 2;
        double valueAmps = Amp_position*1000 + jumpval1;
        if (valueAmps < 126)
        {
            //printf("the limited current position is: %f \n",valueAmps);
            return valueAmps/1000;
        }
        else
        {
            return 0;
        }
    }
    else if (limit ==1)
    {
        float jumpval2 = ((float)rand())/RAND_MAX * 1.5 + 0.9;
        double valueAmps = Amp_position*1000 + jumpval2;
        if (valueAmps < 126)
        {
            //printf("the limited current position is: %f \n",valueAmps);
            return valueAmps/1000;
        }
        else
        {
            return 0;
        }
    }
}


void ACO_main (struct ACOR_Parameters *values,
               struct empty_individual *InitialAnt,
               struct bestSolutions *topValues,struct reference_data *Data)
{
    //repeat the main loop for maximum number of iteration
    for (int it = 0; it<values->MaxIt; it++)
    {
        //create two arrays that hold voltage and current values
        double best_Voltage [values->bestNum];
        double best_Current [values->bestNum];
        //now we need to calculate the distance between each best solution
        for (int i = 0; i < values->bestNum; i++)
        {
          best_Voltage[i] = abs(topValues->bestVolts[i]-topValues->bestVolts[0]);
          best_Current[i] = abs(topValues->bestAmps[i]-topValues->bestAmps[0]);
          //printf("best voltage distance: %f \n",best_Voltage[i]);
          //printf("best current distance: %f \n",best_Current[i]);
        }

        //next step is to create an array that hold Guassian values for each position
        double Guassian_volts [values->bestNum];
        double Guassian_amps [values->bestNum];

        //now calculate the Guassian value for each distance
        for (int i = 0; i<values->bestNum; i++)
        {
            Guassian_volts[i] = exp(-(pow(best_Voltage[i],2))/(2*values->t));
            Guassian_amps[i] = exp(-(pow(best_Current[i],2))/(2*values->t));
            //printf("Guassian volt is: %f and %f \n",Guassian_volts[i],Guassian_amps[i]);
        }

        //summing all the guassian values
        double sum_Guassian_volt = 0;
        double sum_Guassian_amps = 0;
        for (int i = 0; i <values->bestNum; i++)
        {
            sum_Guassian_volt = sum_Guassian_volt + Guassian_volts[i];
            sum_Guassian_amps = sum_Guassian_amps + Guassian_amps [i];
        }

        //printf("print the sum of current : %f and voltage : %f \n",sum_Guassian_amps, sum_Guassian_volt);

        //now we need to create an array that hold phermone values for each solution
        double Phermone_volts [values->bestNum];
        double Phermone_amps [values->bestNum];

        for (int i = 0 ; i <values->bestNum; i++)
        {
            Phermone_volts[i] = Guassian_volts[i]/sum_Guassian_volt;
            Phermone_amps[i] = Guassian_amps[i]/sum_Guassian_amps;
            //printf("phermone stuff: %f \n",Phermone_volts[i]);
        }

        //create a reference struct to hold all the relevant information
        //about the new start location of each ant
        for (int i = 0; i <values->bestNum; i++)
        {
            Data->number_of_ants[i] = Phermone_volts[i] * values->new_Ants;
            Data->referencePos1[i] = topValues->bestVolts[i];
            Data->referencePos2[i] = topValues->bestAmps[i];
            printf("number of ants starting at each location : %f : %f : %f \n", Data->number_of_ants[i],
                   Data->referencePos1[i], Data->referencePos2[i]);
        }

        struct empty_individual new_Ant_population;

        int start = 0;

        for (int i = 0 ; i < values->bestNum; i++)
        {
            int numberAnts = Data->number_of_ants[i] + start;
            //printf("printing the start value: %d \n",start);
            for (int j = start; j<numberAnts; j++)
            {
                // random value of either 0 or 1 which determines the jump to
                //left or right hand side of the curve
                int random_jump = (rand() %(2) + 0);
                new_Ant_population.Position1[j] = check_Position1(Data->referencePos1[i],random_jump);
                new_Ant_population.Position2[j] = check_Position2(Data->referencePos2[i],random_jump);
                //now we need to obtain the new voltage and current position,
                //however this is where we need to add limits in oder to make sure that
                //it stays within the limits.
                double volt = new_Ant_population.Position1[j];
                double amps = new_Ant_population.Position2[j];
                new_Ant_population.Cost[j] = fitness_function(volt, amps);
                //now calculating the duty cycle  for each ants according to the equation (7)
                new_Ant_population.duty [j] = values->reference_voltage / new_Ant_population.Position1[j];

                printf("the new power values: %f: %f: %f: duty cycle: %f \n",new_Ant_population.Cost[j],new_Ant_population.Position1[j],
                       new_Ant_population.Position2[j],new_Ant_population.duty[j]);
            }
            start = start + Data->number_of_ants[i];
        }

        //combining the previous best values with the new best values
        // initalise an array at every iteration to combine the best values at current
        // iteration with the overall best solutions
          int total_array_length = values->new_Ants + values->bestNum;
          double total_value_power [total_array_length];
          double total_value_voltage [total_array_length];
          double total_value_current [total_array_length];
          double total_value_dutyCycle [total_array_length];

         //putting the top values and the new values into one array
         int tracking = 0;
         for (int i = 0; i < values->bestNum; i++)
         {
             total_value_power [i] = topValues->bestWatts[i];
             total_value_voltage [i] = topValues->bestVolts[i];
             total_value_current [i] = topValues->bestAmps[i];
             total_value_dutyCycle [i] = topValues->bestDutyCycle[i];
             tracking++;
         }

         for (int i = 0; i < values->new_Ants; i++)
         {
             total_value_power [tracking] = new_Ant_population.Cost[i];
             total_value_voltage [tracking] = new_Ant_population.Position1[i];
             total_value_current [tracking] = new_Ant_population.Position2[i];
             total_value_dutyCycle [tracking] = new_Ant_population.duty[i];
             //printf("the totalvalue duty: %f\n",total_value_dutyCycle[tracking]);
             tracking++;
         }

        //next step is to sort the new combined array from max to min
        //create a temp array to hold maximum value of the power
          double temp [total_array_length];
          double temp1 [total_array_length];
          double temp2 [total_array_length];
          double temp3 [total_array_length];


         int array_numb;

         //intilise the temp array with zeros
         for (int i = 0; i<total_array_length; i++)
         {
             temp[i] = 0;
             temp1[i] =0;
             temp2[i] =0;
             temp3[i] =0;
             //printf("the temp array: %f \n",temp[i]);
         }

          //now next step is to sort the struct from highest value to lowest value
          for (int i = 0; i<total_array_length; ++i)
          {
              for (int j =0; j<total_array_length; ++j)
              {
                  if (temp[i] >= total_value_power[j] )
                  {
                      //printf("the value is bigger than previous \n");
                  }
                  else if (temp[i] < total_value_power[j])
                  {
                      temp[i] = total_value_power[j];
                      temp1[i] = total_value_voltage[j];
                      temp2[i] = total_value_current[j];
                      temp3[i] = total_value_dutyCycle[j];
                      array_numb = j;
                      //printf("array numb is: %d \n", array_numb);
                  }
              }

             // next we need to remove the highest value from the array
            int length = sizeof (total_value_power)/ sizeof (double);
            //printf("the length of the array is: %d \n", length);
            for (int x = array_numb; x < length; x++)
            {
               total_value_power[x] = total_value_power[x+1];
               total_value_voltage[x] = total_value_voltage[x+1];
               total_value_current[x] = total_value_current[x+1];
               total_value_dutyCycle[x] = total_value_dutyCycle[x+1];
            }
          }

          //now that the power is arranged from min to max we can save it back to the orginal
          //array
          for (int x = 0; x<values->new_Ants; x++)
          {
              total_value_power[x] = temp[x];
              total_value_voltage[x] = temp1[x];
              total_value_current[x] = temp2[x];
              total_value_dutyCycle[x] = temp3[x];
              printf("printing the new ant values is: %f =  %f *  %f and duty: %f \n",total_value_power[x],
                      total_value_voltage[x],total_value_current[x],total_value_dutyCycle[x]);
          }

          //now put the top values into the struct and repeat the process
          //put the best values into this struct
          for (int x = 0; x < values->bestNum; x++)
          {
              topValues->bestWatts[x] = total_value_power[x];
              topValues->bestVolts[x] = total_value_voltage[x];
              topValues->bestAmps[x] = total_value_current[x];
              topValues->bestDutyCycle[x] = total_value_dutyCycle[x];
              printf("best updated values: %f =  %f *  %f and duty cycle is: %f\n",topValues->bestWatts[x],
                     topValues->bestVolts[x],topValues->bestAmps[x],topValues->bestDutyCycle[x]);
          }

    }
    //after the iteration select the top best voltage value
    //values->mpptVolt = topValues->bestVolts[0];
}
