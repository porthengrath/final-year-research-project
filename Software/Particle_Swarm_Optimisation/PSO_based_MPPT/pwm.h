/*
 * pwm.h
 *
 *  Created on: 16 May 2019
 *      Author: Portheng
 */

#ifndef PWM_H_
#define PWM_H_

#include "Arduino.h"

// use timer 2
class pwm
{
    static const uint8_t PWM_PIN = PD3;
    static const double  DEFAULT_STEP_SIZE = 0.625;

public:
                         pwm();
    void                 setup_pwm();
    void                 turn_on_timer();
    void                 turn_off_timer();
    void                 set_duty_cycle(double duty_percent);
};



#endif /* PWM_H_ */
