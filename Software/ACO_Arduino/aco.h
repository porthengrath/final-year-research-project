#ifndef ACO_H_INCLUDED
#define ACO_H_INCLUDED

// Struct that contains all the parameters information we need
struct ACOR_Parameters
{
  //number of ants
  int number_Ants;
  //number of iteration
  int MaxIt;
  //number of best values
  int bestNum;
  //the number of new population
  int new_Ants;
  //Standard deviation
  double t;
  //size of the solution archive
  int numSol;
  //start value
  int start;
  //The output or reference voltage
  double reference_voltage;
  // MPP voltage (best possible voltage)
  double mpptVolt;
  //Counter, that keeps track of how many iteration have been completed
  int Counter;
  //the dimension of the problem, in our case its two, voltage and current
  int nVar;
  //Matrix that holds solar pannel voltage and current
  double solar_data [10][2];
};

//This struct contain the Position and cost of each ant
struct empty_individual
{
  double Position1 [10];
  double Position2 [10];
  double Cost [10];
  double duty [10];
};

//struct that hold the best solution from
struct bestSolutions
{
    //not the best way to do this need to reserach more into this
    double bestVolts[5];
    double bestAmps[5];
    double bestWatts[5];
    double bestDutyCycle[5];
};

struct reference_data
{
    double number_of_ants[5];
    double referencePos1[5];
    double referencePos2[5];
};

double fitness_function (double voltage, double current);

void start_initial_population (struct ACOR_Parameters *values,
                               struct empty_individual *InitialAnt);

void Solution_archive(struct bestSolutions *topValues,
                      struct ACOR_Parameters *values,
                      struct empty_individual *InitialAnt);

void update_duty_cycle (struct ACOR_Parameters *values);

void ACO_main (struct ACOR_Parameters *values,
               struct empty_individual *InitialAnt,
               struct bestSolutions *topValues,struct reference_data *Data);



#endif // ACO_H_INCLUDED
